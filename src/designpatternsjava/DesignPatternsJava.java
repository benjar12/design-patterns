/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package designpatternsjava;

import designpatternsjava.factoryDesignPattern.FactoryMain;

/**
 *
 * @author benjaminjarman
 */
public class DesignPatternsJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Run Factory Pattern Code
        FactoryMain.main(args);
    }
    
}
