/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package designpatternsjava.factoryDesignPattern;
import java.util.Scanner;

/**
 *
 * @author benjaminjarman
 */
public class FactoryMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EnemyShipFactory shipFactory = new EnemyShipFactory();
        
        EnemyShip theEnemy = null;
        
        Scanner userInput = new Scanner(System.in);
        
        System.out.println("What type of ship? ( U or R )");
        
        if(userInput.hasNextLine()){
            String typeOfShip = userInput.nextLine();
            
            theEnemy = shipFactory.makeEnemyShip(typeOfShip);
        }
        
        if(theEnemy != null){
            doStuffEnemy(theEnemy);
        }else System.out.println("What type of ship? ( U or R )");
    }
    
    public static void doStuffEnemy(EnemyShip enemyShip){
        enemyShip.displayEnemyShip();
        enemyShip.followHeroShip();
        enemyShip.enemyShipShoots();
    }
    
}
