package designpatternsjava.factoryDesignPattern;



public abstract class EnemyShip {
    private String name;
    private double antDamage;
    
    public String getName() { return name; }
    public void setName(String newName){ name = newName; }
    
    public double getDamage(){return antDamage; }
    public void setDamage(double newDamage){antDamage = newDamage;}
    
    public void followHeroShip(){
        System.out.println(getName() + " is following the hero");
    }
    public void displayEnemyShip(){
        System.out.println(getName() + " is on the screen");
    }
    public void enemyShipShoots(){
        System.out.println(getName() + " attacks and does " + getDamage());
    }
}